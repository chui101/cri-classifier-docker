FROM sklearn
MAINTAINER roger@kcr.uky.edu

RUN pip install flask
WORKDIR /work
COPY . /work
VOLUME /work/models
EXPOSE 5000

CMD ["python2.7","app.py","--listen=0.0.0.0"]
