# Automated Cancer ML

## Required Packages
- numpy
- scipy
- scikit-learn

# Examples

## Training a new model

This trains a model on the dataset reuters.json and saves the model in tmp.pkl.

~~~~
$ python cancer_robot.py --repeat 2  --train ./data/reuters.json  model
~~~~

See sample output in ./data/sample_train_output.json

## Testing a new model

This generates predictiosn on reuters.json, saves the results in tmp2.json and
uses the model tmp.pkl

~~~~
$ python cancer_robot.py --test ./data/reuters.json preds model.pkl
~~~~

See sample output in ./data/sample_test_output.json

## Get information about command line parameters

~~~~
$ python cancer_robot.py --help
usage: cancer_robot.py [-h] (--train TRAIN | --test TEST) [--repeat REPEAT]
                       [--n_jobs N_JOBS] [--stratified]
                       output_filename [test_model]

Automate text classification.

positional arguments:
  output_filename  the filename to be used for training or testing (JSON
                   format and pkl for training)
  test_model       name of the model to use for testing

optional arguments:
  -h, --help       show this help message and exit
  --train TRAIN    the model name
  --test TEST      the pickled model name to use for testing
  --repeat REPEAT  the number of times to repeat k-fold cross-validation
                   (default: 10)
  --n_jobs N_JOBS  the number of concurrent threads to use for training
                   (default: 1)
  --stratified     Use stratified sampling for cross-validation
~~~~
