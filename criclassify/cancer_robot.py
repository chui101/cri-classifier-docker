import os
import sys
import json
import argparse
from time import time

import numpy as np
import scipy as sp
import scipy.stats
from sklearn.cross_validation import StratifiedShuffleSplit, ShuffleSplit
from sklearn.metrics import precision_recall_fscore_support
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfVectorizer

from src.Model import Model

def load_json(filename, is_training):
    data = []
    data_ids = []
    y = None
    with open(filename,'r') as in_file:
        json_data = json.load(in_file)
    for row in json_data:
        data.append(row['data'])
        data_ids.append(row['id'])
        if is_training:
            if y is None:
                y = [row['class']]
            else:
                y.append(row['class'])
    return data_ids, data, y

def to_dict(test_ids, preds, preds_proba, classes):
    data = []
    for i, p, pp in zip(test_ids, preds, preds_proba):
        class_probs = {}
        for c,cp in zip(classes, pp.flatten()):
            class_probs[c] = cp
        data.append({'id':i, 'pred':p, 'proba':class_probs})
    return data

def mean_confidence_interval(data, confidence=0.95):
    n = data.shape[0]
    m, se = np.mean(data), scipy.stats.sem(data)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m-h, m+h

def train(train_data, train_classes, repeat, use_stratified, n_jobs):
    best_c = None
    best_score = -99999
    best_precision = None
    best_recall = None
    best_f1_score = None
    best_per_class_precision = None
    best_per_class_recall = None
    best_per_class_f1_score = None
    classes_set_list = list(set(train_classes))
    #for c in [1e-3, 1e-2, 1e-1, 1., 10.]:
    for c in [1.]:
        per_class_precision = []
        per_class_recall = []
        per_class_f1_score = []
        micro_precision = []
        micro_recall = []
        micro_f1_score = []
        for r in range(repeat):
            if use_stratified:
                cv = StratifiedShuffleSplit(train_classes, n_iter=1, test_size=0.2)
            else:
                cv = ShuffleSplit(len(train_classes), n_iter=1, test_size=0.2)

            pred_classes = None
            true_classes = None
            for train_index, test_index in cv:
                X_train = train_data[train_index]
                X_test = train_data[test_index]
                y_train = np.array(train_classes)[train_index]
                y_test = np.array(train_classes)[test_index]

                model = Model(c, n_jobs, classes_set_list)
                model.fit(X_train, y_train)
                y_test_preds = model.predict(X_test)
                
                if pred_classes is None:
                    pred_classes = y_test_preds
                    true_classes = y_test
                else:
                    pred_classes = np.concatenate([pred_classes, y_test_preds])
                    true_classes = np.concatenate([true_classes, y_test])

            precision, recall, f1_score,_ = precision_recall_fscore_support(true_classes, pred_classes, average=None, labels=classes_set_list)
            per_class_precision.append(precision)
            per_class_recall.append(recall)
            per_class_f1_score.append(f1_score)
            precision, recall, f1_score,_ = precision_recall_fscore_support(true_classes, pred_classes, average='micro')
            micro_precision.append(precision)
            micro_recall.append(recall)
            micro_f1_score.append(f1_score)
        new_f1_score = np.mean(micro_f1_score)
        if new_f1_score > best_score:
            best_c = c
            best_per_class_precision = np.array(per_class_precision)
            best_per_class_recall = np.array(per_class_recall)
            best_per_class_f1_score = np.array(per_class_f1_score)
            best_micro_precision = np.array(micro_precision)
            best_micro_recall = np.array(micro_recall)
            best_micro_f1_score = np.array(micro_f1_score)
            
    final_mod = Model(best_c, n_jobs, classes_set_list) 
    final_mod.fit(train_data, train_classes)

    avg_best_precision = best_micro_precision.mean()
    avg_best_recall = best_micro_recall.mean()
    avg_best_f1_score =  best_micro_f1_score.mean()
    avg_best_f1_score_conf = mean_confidence_interval(best_micro_f1_score, 0.95)

    avg_best_per_class_precision = best_per_class_precision.mean(axis=0)
    avg_best_per_class_recall = best_per_class_recall.mean(axis=0)
    avg_best_per_class_f1_score = best_per_class_f1_score.mean(axis=0)

    per_c_prec = {}
    per_c_recall = {}
    per_c_f1_score = {}
    for i,x in enumerate(classes_set_list):
        per_c_prec[x] = avg_best_per_class_precision[i]
        per_c_recall[x] = avg_best_per_class_recall[i]
        per_c_f1_score[x] = avg_best_per_class_f1_score[i]

    return final_mod, best_c, avg_best_precision, avg_best_recall, avg_best_f1_score, \
           per_c_prec, per_c_recall, \
           per_c_f1_score, avg_best_f1_score_conf

def main():
    parser = argparse.ArgumentParser(description='Automate text classification.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--train', dest='train', action='store',
                        help='the model name')
    group.add_argument('--test', dest='test', action='store',
                        help='the pickled model name to use for testing')
    parser.add_argument('--repeat', dest='repeat', default=10,
                        help='the number of times to repeat k-fold cross-validation (default: 10)')
    parser.add_argument('--n_jobs', dest='n_jobs', default=1,
                        help='the number of concurrent threads to use for training (default: 1)')
    parser.add_argument('--stratified', dest='stratified', action='store_true',
                        help='Use stratified sampling for cross-validation')
    parser.add_argument('output_filename', help='the filename to be used for training or testing (JSON format and pkl for training)')
    parser.add_argument('test_model', nargs='?', default=os.getcwd(), help='name of the model to use for testing')

    args = parser.parse_args()

    if args.train is not None:
        try:
            t0 = time()
            _, train_data, train_classes = load_json(args.train, True)
            train_data = np.array(train_data, dtype=str)
            train_classes = np.array(train_classes, dtype=str)
            train_data = train(train_data, train_classes, int(args.repeat), args.stratified, int(args.n_jobs))
            clf = train_data[0]
            clf.save(args.output_filename)
            data = {}
            data['C'] = train_data[1]
            data['micro_precision'] = train_data[2]
            data['micro_recall'] = train_data[3]
            data['micro_f1_score'] = train_data[4]
            data['per_class_precision'] = train_data[5]
            data['per_class_recall'] = train_data[6]
            data['per_class_f1_score'] = train_data[7]
            data['micro_f1_lower_conf'] = train_data[8][0]
            data['micro_f1_higher_conf'] = train_data[8][1]
            with open(args.output_filename+'.json', 'w') as out_file:
                json.dump(data, out_file)
            print 'Finished Training in %d seconds.' % (time()-t0)
        except Exception, e:
            print >> sys.stderr, "Exception: %s" % str(e)
            sys.exit(1)
    else:
        try:
            model = Model()
            model.load(args.test_model)
            test_ids, test_data, _ = load_json(args.test, False)
        except Exception, e:
            print >> sys.stderr, "Exception: %s" % str(e)
            sys.exit(1)
        preds = model.predict(test_data)
        preds_proba = model.predict_proba(test_data)
        test_dict = to_dict(test_ids, preds, preds_proba, model.train_classes)
        with open(args.output_filename+'.json', 'w') as out_file:
            json.dump(test_dict, out_file)

if __name__ == '__main__':
    main()
