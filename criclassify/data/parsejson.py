import json
import glob

new_data = []
for filename in glob.glob('./data/full/*.json'):
    with open(filename) as in_file:
        data = json.load(in_file)

    for d in data:
        new_d = {}
        if 'topics' not in d: 
            continue 
        if 'body' not in d:
            continue
        new_d['id'] = d['id']
        new_d['data'] = d['body'].encode('ascii','ignore')
        new_d['class'] = d['topics'][0]
        new_data.append(new_d)
print len(new_data)
with open('reuters.json','w') as out_file:
    json.dump(new_data, out_file)
