import numpy as np
from scipy.special import expit
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.externals import joblib

class Model:
    def __init__(self, C=1., n_jobs=1, train_classes=None):
        self.C = C
        self.svc = LinearSVC(C=self.C, dual=False, penalty='l1')
        #self.svc = LinearSVC(C=self.C)
        self.clf = OneVsRestClassifier(self.svc, n_jobs=n_jobs)
        self.vec = TfidfVectorizer(min_df=5, norm='l2', ngram_range=(1,2))
        self.train_classes = train_classes

    def fit(self, train_data, train_classes):
        train_matrix = self.vec.fit_transform(list(train_data))
        self.clf.fit(train_matrix, [str(x) for x in list(train_classes)])

    def predict(self, test_data):
        test_matrix = self.vec.transform(test_data)
        return self.clf.predict(test_matrix)

    def predict_proba(self, test_data):
        test_matrix = self.vec.transform(test_data)
        scores = self.clf.decision_function(test_matrix)
        #return expit(scores)
        #return scores
        return 1.0 / (1.0 + np.exp(-1.0 * scores))

    def load(self, filename):
        loaded_model = joblib.load(filename)
        self.clf = loaded_model['model']
        self.vec = loaded_model['vectorizer']
        self.train_classes = self.clf.classes_

    def save(self, filename):
        joblib.dump({'model':self.clf, 'vectorizer':self.vec, 'train_classes':self.train_classes}, filename+'.pkl', compress=True)
