#!/usr/bin/python2.7

from flask import Flask,render_template,request,Response
from criclassify import classify as cc
import argparse
import json

app = Flask(__name__)

@app.route("/")
def helloWorld():
    return render_template("index.html")

@app.route("/classify",methods=['GET','POST'])
def classify():
    if request.method == 'POST':
        args = {}
        data = {}
        data['id'] = 1
        data['data'] = request.form['article-text']
        test_model = 'models/model.pkl'
        test_data = '[' + json.dumps(data) + ']'
        result = cc.classify(test_model,test_data)
        return Response(result,mimetype='application/json')
    elif request.method == 'GET':
        return render_template("form.html")
    return "Unsupported method: %s" % request.method

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='starts the flask wsgi server for the predictor')
    parser.add_argument('--listen',nargs='?',default='127.0.0.1',help='IP to listen on')
    parser.add_argument('--port',nargs='?',default='5000',help='port to listen on')
    args = parser.parse_args()

    app.run(threaded=True, host=args.listen, port=args.port)
